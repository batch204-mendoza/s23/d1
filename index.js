console.log("hello");

/*
objects
	an object is a data type that is used to represent real world object

	it is  a collection of releated data and/or functionalities

	information is stored in object represented in key value pairs.

	key - property of the object
	value- actual data stored

two ways of creating objects in javascropt
1. object literal Notation
let/ const objectName ={}

2. object constructor notation
	object instantiation (let object = new objecty)
object litral Notation

Syntax:
let= {
	 keyA: value,
	 keyB: value,
}


*/


let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};
console.log("result of making objects using a literal notation")
console.log(cellphone)

console.log(typeof cellphone)

let cellphone2={
	name: "motorola",
	manufactureDate: "2000"
};

/*creating onjects using constructor function

syntax:
function Object name (keyA, Keyb){
	this.keyA =keyA;
	this.KeyB= keyB;
}
*/




function Laptop (name, manufactureDate)
	{
	// this.propertyname=value
	this.name = name ;
	this.manufactureDate = manufactureDate ;
	} ;
// let laptop = new Laptop ("Lenovo" , 2008)
// console.log (laptop) ;


// this is a unique instance of laptop object

/*
this new opeperator creastes an instance of an object 
-object and instances are often interchanged because object literals 
(let object= {}) and instance (let object = new object) are distinct/ unique objects
*/

let laptop = new Laptop("lenovo", 2008);
console.log(laptop)

let oldLaptop=Laptop("ibm", 1980);
console.log(oldLaptop);// undefined


let myLaptop = new Laptop("Macbook", 2020);

console.log(myLaptop);

//creating empty objects
let computer ={};
console.log(computer);

let myComputer = new Object();
console.log(myComputer)

// accessing Object Properties


// using the dot notation
console.log(myLaptop.name)
console.log(laptop.manufactureDate);
console.log(myLaptop.manufactureDate);

//using the square bracket method
console.log(myLaptop["name"])
console.log(myLaptop["manufactureDate"])

//accessing array Objects
let array= [laptop,myLaptop];

//let array= [{name:"lenovo", manufactureDate:2008}] same as 107.

//arrayName[indexNumber].propertyName
// this tells us the array[1] is an object by using the dot
console.log(array[1].name)
console.log(array[1]["name"])

//initializing/ adding/deleting/reassinging object properties. 

let car = {};
console.log(car);
// objectname.propertyName =Value
car.name = "Sarao";
console.log("Result from adding properties using .notation")
console.log(car);
car["manufacture Date"]=2019;
console.log(car);

car["chasis"]="4wheel";
console.log(car);

// delete is for delete properties in objects
// when you remove the property you remove the value as well
delete car["manufacture Date"];

console.log(car);

car.manufactureDate=2019;
console.log(car);


//reassigning object properties
car.name ="Mustang";
console.log(car);

//object methods
/*
A method is a function which is a property of an object
they are also functions- key differences they have from other functions but methods are functions are related to a specific object
-methods are useful for creating object specific functions which are used to perform  tasks on them. 


*/

let person= {
	//property
	name: "Jack",
	//method
	talk: function(){
		console.log("hello! my name is" + this.name);
	}
}

console.log(person);
// invoking the method
person.talk();

person.walk= function(){
	console.log(this.name+ " walked 25 steps forward.")
}

person.walk();

//methods are useful for creating reasuable function that performs task related to our object. 

let friend = {
	firstName: "rafael",
	lastName: "santillian",
	//isMarried: false,
	address: {
		city: "quezon city",
		country: "philippines"
	},
	email: ["raf@mail.com", "raf123@yahoo.com"],
	introduce: function(){
		console.log("hello, My Name is "+this.firstName+ " "+ this.lastName);
	}
	}

friend.introduce();

// real world application
/*
scenario 
	1. we would like to create a game that would have several pokemon that will interact with each other
	2. every pokemon would have the same set of stats, properties , functions

*/

let myPokemon = {
	name: "pikachu",
	level: 3,
	health: 100,
	attack: 50,

	tackle: function () {
		console.log("this pokemon tackle target pokemon");
		console.log("targePokemons's health is now reduced to to target pokemonHealth")
	},

	faint: function(){
		console.log("pokemon fainted")
	}

}
console.log(myPokemon)

// object constructo for pokemon

/*function Pokemon(name,level){
this.name= name;
this.level=level;
this.health=level*3;
this.Attack=level*5;

//methods
this.tackle=function(target){
	console.log(this.name+ "tackled"+ target.name);
	console.log(target.name+" 's is now reduced to "+ (target.health - this.attack));
},

this.faint=function (){
	console.log(this.name+"fainted");
}
}
*/
function Pokemon(name, level, health) {
	//properties
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health=target.health-this.attack
		if (target.health<=0)
		target.faint(target)
		
	},

	


	this.faint = function() {
		

		console.log(this.name + " fainted");
	}
}
let squirtle = new Pokemon("squirtle" ,99,200)
let snorlax= new Pokemon("snorlax", 75,500);
console.log(squirtle);
console.log(snorlax);

snorlax.tackle(squirtle);